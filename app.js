import WebTorrent from 'webtorrent'
import fs from 'fs'

const client = new WebTorrent()

for (const torrent of JSON.parse(fs.readFileSync('torrents.json'))) {
  if (!torrent[2]) client.add(torrent[1], { path: './downloads' })
}

setInterval(() => {
  console.clear()
  client.torrents.forEach(t => {
    console.log(`
${(Math.round(t.progress * 100) + '').padStart(3, ' ')}% | ${t.name}`)
  })
}, 999)
